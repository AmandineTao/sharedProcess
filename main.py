import multiprocessing


def print_element(elements):
    # print each list element
    for element in elements:
        print(f"Name: {element[0]} \nScore: : {element[1]}\n")


def insert_element(element, elements):
    # insert new element in list
    elements.append(element)
    print(f"New element {element} is added.\n")


def remove_element(elements, rm_element=('citron', 'vert')):
    # remove element from list
    for element in elements:
        if element[0] == rm_element[0] and element[1] == rm_element[1]:
            elements.remove(element)
            print(f"element {element} is removed.\n")


if __name__ == '__main__':
    with multiprocessing.Manager() as manager:

        # create list in memory of server process
        elements = manager.list([("banane", "jaune"), ("citron", "vert"), ("orange", "orange")])

        # add new element
        new_element = ("goyave", "vert")

        # create processes
        p1 = multiprocessing.Process(target=remove_element, args=(elements,))
        p2 = multiprocessing.Process(target=insert_element, args=(new_element, elements))
        p3 = multiprocessing.Process(target=print_element, args=(elements,))

        # execute process p1 to remove ('citron', 'vert'), and wait until it is executed
        p1.start()
        p1.join()

        # execution process p2 to insert new_element, and wait until it is executed
        p2.start()
        p2.join()

        # execution process p3 to print list element, and wait until it is executed
        p3.start()
        p3.join()